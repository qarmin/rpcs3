### Compilation in Docker

```
docker run -it ubuntu:18.04 /bin/bash

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update
apt install -y git
git clone https://github.com/RPCS3/rpcs3.git
cd rpcs3

#Z#
apt install -y software-properties-common sudo wget

apt install -y build-essential libasound2-dev libpulse-dev libopenal-dev libglew-dev zlib1g-dev libedit-dev libvulkan-dev libudev-dev git libevdev-dev libsdl2-dev libsdl2-dev cmake libglew-dev

git submodule update --init

ucodename=$(lsb_release -sc)
sudo add-apt-repository -y ppa:beineri/opt-qt-5.14.1-$ucodename
sudo apt-get update
#. /opt/qt514/bin/qt514-env.sh >/dev/null 2>&1
sudo apt-get install -y qt514-meta-minimal qt514svg


sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt-get install -y gcc-9 g++-9

wget -qO - http://packages.lunarg.com/lunarg-signing-key-pub.asc | sudo apt-key add -
sudo wget -qO /etc/apt/sources.list.d/lunarg-vulkan-1.2.135-bionic.list http://packages.lunarg.com/vulkan/1.2.135/lunarg-vulkan-1.2.135-bionic.list
sudo apt update
sudo apt install -y vulkan-sdk

wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | sudo apt-key add -
sudo apt-add-repository -y "deb https://apt.kitware.com/ubuntu/ $(lsb_release -sc) main"
sudo apt-get update
sudo apt-get install -y kitware-archive-keyring
sudo apt-key --keyring /etc/apt/trusted.gpg del C1F34CDD40CD72DA
sudo apt-get install -y cmake

mkdir rpcs3_build
cd rpcs3_build
CXX=g++-9 CC=gcc-9 cmake ../

make -j8

```
### QTCreator Includes
```
/usr/include/exiv2
/usr/lib/x86_64-linux-gnu/glib-2.0/include
```
